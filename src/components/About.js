import React from "react";

export default class About extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <main className="main">
        <div className="about">
          <h1>About</h1>
          <section className="section">
            <h2>Developer</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
              convallis convallis nisl eu varius. Nulla at tellus ut dolor
              aliquet mollis. Nullam purus orci, finibus eget egestas in,
              suscipit et nisl. Nullam eget ipsum quis quam molestie
              ullamcorper. In aliquam, purus convallis convallis elementum,
              ligula dolor posuere odio, nec varius lacus eros ut lacus.
              Phasellus id eros aliquam, semper tellus ut, imperdiet elit. Nam
              lectus arcu, bibendum in tincidunt a, vulputate vel massa. Etiam
              venenatis, enim nec tristique ullamcorper, nibh diam iaculis
              nulla, semper ullamcorper urna massa accumsan mi. Vivamus vel
              faucibus metus. Fusce tortor nunc, sagittis vitae libero sit amet,
              blandit scelerisque tortor. Etiam nec viverra metus. Vivamus porta
              dui neque, ut consequat augue efficitur at.
            </p>
            <p>
              Etiam tempus metus in pharetra aliquet. Donec feugiat finibus est
              at ornare. Vestibulum ut tincidunt neque. Fusce laoreet velit
              augue, et auctor ante gravida a. Praesent in placerat massa, et
              ornare velit. Nunc blandit lacus nisl, ut hendrerit odio dignissim
              vitae. Pellentesque dui enim, dictum eu sem eget, varius convallis
              velit. 
            </p>
          </section>
        </div>
      </main>
    );
  }
}
