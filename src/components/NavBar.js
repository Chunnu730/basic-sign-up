import React from "react";
import { Link } from "react-router-dom";

export default class NavBar extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="nav-box">
        <nav>
          <Link to="/" className="link">Home</Link>
          <Link to="/about" className="link">About</Link>
          <Link to="/signup" className="link">Sign up</Link>
        </nav>
      </div>
    );
  }
}
