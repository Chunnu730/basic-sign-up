import React from "react";
import banner from "../images/banner.jpg";
import About from "./About";
export default class Home extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <main className="main">
        <img src={banner} className="banner" alt="banner here" />
        <About />
      </main>
    );
  }
}
