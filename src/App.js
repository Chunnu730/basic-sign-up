import React from "react";
import { Routes, Route } from "react-router-dom";
import Form from "./components/Form";
import NavBar from "./components/NavBar";
import Home from "./components/Home";
import About from "./components/About"

export default class App extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} exact />
          <Route path="/about" element={<About />} />
          <Route path="/signup" element={<Form />} />
        </Routes>
      </>
    );
  }
}
